
require('./bootstrap');

window.Vue = require('vue');
import axios from 'axios';

import VueRouter from 'vue-router'
import Example from './components/ExampleComponent'
import {routes} from './routes' // route file include

Vue.use(VueRouter) // form router view,link support

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('post-component', require('./components/PostComponent.vue').default);
Vue.component('admin-main', require('./components/AdminMain.vue').default);


const router = new VueRouter({
    routes, // short for `routes: routes`
    mode:'history',

})


 const app = new Vue({
    el: '#app',
    router : router,
   
 // render: h => h(AppView),
    });
