
 import Vue from 'vue';
 window.Vue = Vue;
import axios from 'axios';
import moyna from './components/PostComponent'
import new_post from './components/NewPostComponent'
import AdminMain from './components/AdminMain'
Vue.component('moyna', require('./components/PostComponent.vue').default);
Vue.component('new_post', require('./components/NewPostComponent.vue').default);
export const routes = [
    /*{
        path:'/app//home',
        component: Post,
    },*/
    {
        path:'/show-posts',
        component: moyna,
    },
    {
        path:'/add-post',
        component: new_post,
    },
    

];