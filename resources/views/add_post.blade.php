@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center">Blogs</h2>
            <a href="{{route('show-posts')}}" class="btn btn-success btn-md float-right mb-3">Blog List</a>
        </div>
       <new_post></new_post>
    </div>
</div>
@endsection
