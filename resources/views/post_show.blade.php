@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center">Blogs</h2>
            <a href="{{route('post-create')}}" class="btn btn-primary btn-md float-right mb-3">Add New</a>
        </div>
       <moyna></moyna>
    </div>
</div>
@endsection
