<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('abcd');
Route::get('/posts', 'PostController@index')->name('posts');
Route::get('/show-posts', 'PostController@show')->name('show-posts');
Route::get('/add-post', 'PostController@create')->name('post-create');
Route::post('/add-post/store', 'PostController@store')->name('post-store');
Route::get('/post/edit/{id}', 'PostController@edit')->name('post-edit');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

